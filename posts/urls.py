from django.urls import path

from posts.views import show_post_detail, PostListView, PostDetailView

urlpatterns = [
    path("<int:pk>/", PostDetailView.as_view, name="PostDetailView"),
    path("", PostListView.as_view(), name="PostListView"),
]
